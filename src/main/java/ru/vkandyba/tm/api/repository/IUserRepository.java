package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeByLogin(String login);

}
