package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeByLogin(String login);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User create(String login, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User setPassword(String userId, String password);

    boolean isLoginExists(String login);
}
