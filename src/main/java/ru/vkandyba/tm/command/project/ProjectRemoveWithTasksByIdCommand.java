package ru.vkandyba.tm.command.project;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.util.TerminalUtil;

public class ProjectRemoveWithTasksByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "remove-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        serviceLocator.getProjectTaskService().removeById(projectId);
    }

}
