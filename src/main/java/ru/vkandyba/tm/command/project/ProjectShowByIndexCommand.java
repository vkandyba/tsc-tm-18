package ru.vkandyba.tm.command.project;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.List;

public class ProjectShowByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!serviceLocator.getProjectService().existsByIndex(index)) {
            throw new ProjectNotFoundException();
        }
        final Project project = serviceLocator.getProjectService().findByIndex(index);
        final List<Project> projects = serviceLocator.getProjectService().findAll();
        System.out.println("Index: " + projects.indexOf(project));
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}
