package ru.vkandyba.tm.comparator;

import ru.vkandyba.tm.api.entity.IHasStartDate;

import java.util.Comparator;

public class ComparatorByStartDate implements Comparator<IHasStartDate> {

    private static ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    private ComparatorByStartDate(){

    }

    public static ComparatorByStartDate getInstance() {return INSTANCE;}

    @Override
    public int compare(IHasStartDate o1, IHasStartDate o2) {
        if(o1 == null || o2 ==null) return 0;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
