package ru.vkandyba.tm.exception.empty;

import ru.vkandyba.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is Empty!");
    }

}
