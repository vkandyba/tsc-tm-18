package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(String id) {
        for (User user : users) {
            if (id.equals(user.getId()))
                return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (User user : users) {
            if (login.equals(user.getLogin()))
                return user;
        }
        return null;
    }

    @Override
    public User removeUser(User user) {
        return null;
    }

    @Override
    public User removeByLogin(String login) {
        return null;
    }

}
